//Hook up the tweet display

$(document).ready(function () {

    $('#countdown').countdown('2015/12/15').on('update.countdown', function (event) {
        var $this = $(this).html(event.strftime(''
            + '<li><span class="days">%-D</span> <h3>day%!d</h3> </li>'
            + '<li><span class="hours">%H</span> <h3>hr</h3> </li>'
            + '<li><span class="minutes">%M</span> <h3>min</h3> </li>'
            + '<li><span class="seconds">%S</span> <h3>sec</h3></li>'));
    });
});	
